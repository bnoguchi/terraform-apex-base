output "apex_lambda_function_role_arn" {
  value = "${aws_iam_role.apex_lambda_function.arn}"
}

output "apex_lambda_function_role_name" {
  value = "${aws_iam_role.apex_lambda_function.name}"
}
