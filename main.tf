provider "aws" {}

# Let lambda assume this role
# Policy is from https://github.com/apex/apex/blob/master/boot/boot.go#L46-L57
resource "aws_iam_role" "apex_lambda_function" {
  name = "${var.apex_project_name}_lambda_function"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

# Policy is from https://github.com/apex/apex/blob/master/boot/boot.go#L59-L70
resource "aws_iam_policy" "apex_lambda_logs" {
  name        = "${var.apex_project_name}_lambda_logs"
  path        = "/"
  description = "Allow lambda_function to utilize CloudWatchLogs. Mimics creation by apex(1). Created by Terraform"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "attach-policy-apex_lambda_logs" {
  role       = "${aws_iam_role.apex_lambda_function.name}"
  policy_arn = "${aws_iam_policy.apex_lambda_logs.arn}"
}

/*
  See https://docs.aws.amazon.com/lambda/latest/dg/vpc.html

  The Lambda function execution role must have permissions to create, describe
  and delete ENIs. AWS Lambda provides a permissions policy,
  AWSLambdaVPCAccessExecutionRole, with permissions for the necessary EC2
  actions (ec2:CreateNetworkInterface, ec2:DescribeNetworkInterfaces, and
  ec2:DeleteNetworkInterface) that you can use when creating a role. You can
  review the policy in the IAM console. Do not delete this role immediately
  after your Lambda function execution. There is a delay between the time your
  Lambda function executes and ENI deletion. If you do delete the role
  immediately after function execution, you are responsible for deleting the
  ENIs.
 */
resource "aws_iam_role_policy_attachment" "attach-policy-AWSLambdaVPCAccessExecutionRole" {
  role       = "${aws_iam_role.apex_lambda_function.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"
}
